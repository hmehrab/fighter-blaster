//
//  StartScene.swift
//  IOS.Pass
//
//  Created by Mehrab Hasan on 9/24/18.
//  Copyright © 2018 Mehrab Hasan. All rights reserved.
//

import UIKit
import SpriteKit

class StartScene: SKScene {


    var gameLogo: SKLabelNode!
    var gameImage: SKSpriteNode!
    var bg: SKSpriteNode!
    
    var playButton: SKShapeNode!
    
    let scoreKey = "HIGHSCORE"
    override func sceneDidLoad() {
        
        
    }
    
    
    override func didMove(to view: SKView) {
        initializeMenu()
        
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            let touchedNode = self.nodes(at: location)
            for node in touchedNode {
                if node.name == "play_button" {
                    startGame()
                }
            }
        }
    }
    
    func startGame() {
        print("start game")
        gameLogo.run(SKAction.move(by: CGVector(dx: -50, dy: 600), duration: 0.5)) {
            self.gameLogo.isHidden = true
        }
        //2
        playButton.run(SKAction.scale(to: 0, duration: 0.3)) {
            self.playButton.isHidden = true
        }
        //3
        
        
        let transition : SKTransition = SKTransition.fade(withDuration: 0.5)
        let Scene : SKScene = GameScene(size: self.size)
        Scene.scaleMode = .resizeFill
        self.view?.presentScene(Scene, transition: transition)
    }
    
    //3
    func initializeMenu() {
        

        bg = SKSpriteNode(imageNamed: "sky")
        bg.position = CGPoint(x: 0, y: 0)
        bg.size.width = self.frame.size.width
        bg.size.height = self.frame.size.height 
        bg.anchorPoint = CGPoint(x: 0, y: 0)
        addChild(bg)
        
        
        //Create game title
        gameLogo = SKLabelNode(fontNamed: "Zapfino")
        gameLogo.zPosition = 1
        gameLogo.position = CGPoint(x: size.width/2, y: size.height/1.15)
        gameLogo.fontSize = 33
        gameLogo.text = "Fighter Blaster"
        gameLogo.fontColor = SKColor.red
        self.addChild(gameLogo)
        
        //Create Game logo
        
        gameImage = SKSpriteNode(imageNamed: "player")
        gameImage.setScale(0.28)
        gameImage.zPosition = 1
        gameImage.position = CGPoint(x: size.width/2, y: size.height/1.40)
        addChild(gameImage)
        
        let defaults = UserDefaults.standard
        
        let highScore = defaults.integer(forKey: scoreKey)
        
        let bestScore = SKLabelNode(fontNamed: "ArialRoundedMTBold")
        bestScore.zPosition = 1
        bestScore.position = CGPoint(x: size.width/2, y: size.height/2.8)
        bestScore.fontSize = 40
        bestScore.text = "Best Score: \(highScore) "
        bestScore.fontColor = UIColor.red
        self.addChild(bestScore)
        
        // Description
        
        let lb = SKLabelNode(fontNamed: "American Typewriter")
        lb.text = "Protect your territory from the sky. Do not let your enemy go and be careful about incomming. good Luck!"
        lb.numberOfLines = 3
        lb.fontSize = 18
        lb.fontColor = UIColor.black
        lb.preferredMaxLayoutWidth = self.size.width - 40
        lb.zPosition = 1
        lb.position = CGPoint(x: size.width/2, y: size.height/4.1)
        addChild(lb)
        
        
        //Create play button
        playButton = SKShapeNode()
        playButton.name = "play_button"
        playButton.zPosition = 1
        playButton.position = CGPoint(x: size.width/2, y: size.height/5.18)
        playButton.fillColor = SKColor.blue
        let topCorner = CGPoint(x: -30, y: 30)
        let bottomCorner = CGPoint(x: -30, y: -30)
        let middle = CGPoint(x: 30, y: 0)
        let path = CGMutablePath()
        path.addLine(to: topCorner)
        path.addLines(between: [topCorner, bottomCorner, middle])
        playButton.path = path
        self.addChild(playButton)
    }
}
