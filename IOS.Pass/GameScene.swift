//
//  GameScene.swift
//  IOS.Pass
//
//  Created by Mehrab Hasan on 7/11/18.
//  Copyright © 2018 Mehrab Hasan. All rights reserved.
//

import SpriteKit

func +(left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func -(left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func *(point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

func /(point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x / scalar, y: point.y / scalar)
}

#if !(arch(x86_64) || arch(arm64))
func sqrt(a: CGFloat) -> CGFloat {
    return CGFloat(sqrtf(Float(a)))
}
#endif

extension CGPoint {
    func length() -> CGFloat {
        return sqrt(x*x + y*y)
    }
    
    func normalized() -> CGPoint {
        return self / length()
    }
}

class GameScene: SKScene {
    
    struct PhysicsCategory {
        static let none      : UInt32 = 0
        static let all       : UInt32 = UInt32.max
        static let monster   : UInt32 = 0b10       // 1
        static let projectile: UInt32 = 0b11      // 2
        static let bomb      : UInt32 = 0b1      // 3
    }
    
    // 1
    let player = SKSpriteNode(imageNamed: "player")
    
    var background = SKSpriteNode(imageNamed: "background")
    var monstersDestroyed = 0
    var bombDestroyed = 0
    
    override func didMove(to view: SKView) {
        // 2
//        backgroundColor = SKColor.white
        background.position = CGPoint(x: 0, y: 0)
        background.size.width = self.frame.size.width*3
        background.size.height = self.frame.size.height*3
        background.anchorPoint = CGPoint(x: 0.5,y: 0.5)
        background.zPosition = -90
        
        addChild(background)
        // 3
        player.position = CGPoint(x: size.width * 0.5, y:size.height * 0.1)
        player.setScale(0.2)
        // 4
        addChild(player)
        
        physicsWorld.gravity = .zero
        physicsWorld.contactDelegate = self
        
        run(SKAction.repeatForever(
            SKAction.sequence([
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.0),
                SKAction.run(addMonster),
                SKAction.run(addBomb),
                SKAction.wait(forDuration: 2.0),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.0),
                SKAction.run(addMonster),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.5),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.0),
                SKAction.run(addBomb),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.2),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.0),
                SKAction.run(addBomb),
                SKAction.wait(forDuration: 1.8),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 0.5),
                SKAction.run(addBomb),
                SKAction.wait(forDuration: 1.9),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.0),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.4),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 0.5),
                SKAction.run(addBomb),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.0),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.5),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 0.5),
                SKAction.run(addBomb),
                SKAction.wait(forDuration: 2.0),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 1.0),
                SKAction.run(addMonster),
                SKAction.wait(forDuration: 0.5),
                SKAction.run(addBomb)
                
                ])
        ))
        

    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
    
    func addMonster() {
        // Create sprite

        let monster = SKSpriteNode(imageNamed: "fighter\(arc4random_uniform(6))")
        monster.setScale(0.18)
        
        
        monster.physicsBody = SKPhysicsBody(circleOfRadius: monster.size.width/2)
        monster.physicsBody?.isDynamic = true // 2
        monster.physicsBody?.categoryBitMask = PhysicsCategory.monster // 3
        monster.physicsBody?.contactTestBitMask = PhysicsCategory.projectile // 4
        monster.physicsBody?.collisionBitMask = PhysicsCategory.none // 5
        
       
        
        // Determine where to spawn the monster along the x axis
        let actualX = random(min: monster.size.height/8, max: size.width - monster.size.width/6)
        
        

        
        monster.position = CGPoint(x: actualX , y: frame.size.height + 2 )
        
        // Add the monster to the scene
        addChild(monster)
        
        // Determine speed of the monster
        let actualDuration = random(min: CGFloat(4.0), max: CGFloat(8.0))
        
        // Create the actions
        let actionMove = SKAction.move(to: CGPoint(x: actualX , y: -monster.size.width/2), duration: TimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        let loseAction = SKAction.run() { [weak self] in
            guard let `self` = self else { return }
            let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
            let gameOverScene = GameOverScene(size: self.size, won: 1, monstersDestroyed:self.monstersDestroyed)
            self.view?.presentScene(gameOverScene, transition: reveal)
        }
        monster.run(SKAction.sequence([actionMove, loseAction, actionMoveDone]))

    }
    
    // ADD BOMB
    func addBomb() {
        // Create sprite
        
        
        let bomb = SKSpriteNode(imageNamed: "bomb\(arc4random_uniform(3))")
        bomb.setScale(0.17)
        
        
        bomb.physicsBody = SKPhysicsBody(rectangleOf: bomb.size) // 1
     
        bomb.physicsBody?.isDynamic = true // 2
        bomb.physicsBody?.categoryBitMask = PhysicsCategory.bomb// 3
        bomb.physicsBody?.contactTestBitMask = PhysicsCategory.projectile // 4
        bomb.physicsBody?.collisionBitMask = PhysicsCategory.none // 5
        
        // Determine where to spawn the monster along the x axis
        let actualX = random(min: bomb.size.height/8, max: size.width - bomb.size.width/4)
        
        // Position the monster slightly off-screen along the right edge,
        // and along a random position along the Y axis as calculated above
        
        
        bomb.position = CGPoint(x: actualX , y: frame.size.height + 2 )
        
        // Add the monster to the scene
        addChild(bomb)
        
        // Determine speed of the monster
        let actualDuration = random(min: CGFloat(8.0), max: CGFloat(12.0))
        
        // Create the actions
        let actionMove = SKAction.move(to: CGPoint(x: actualX , y: -bomb.size.width/2), duration: TimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        
        bomb.run(SKAction.sequence([actionMove,  actionMoveDone]))
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // 1 - Choose one of the touches to work with
        guard let touch = touches.first else {
            return
        }

        
        let touchLocation = touch.location(in: self)
        
        // 2 - Set up initial location of projectile
        let projectile = SKSpriteNode(imageNamed: "missile")
        projectile.setScale(0.11)
        projectile.position = player.position
        
        projectile.physicsBody = SKPhysicsBody(circleOfRadius: projectile.size.width/3.5)
        projectile.physicsBody?.isDynamic = true
        projectile.physicsBody?.categoryBitMask = PhysicsCategory.projectile
        projectile.physicsBody?.contactTestBitMask = PhysicsCategory.monster | PhysicsCategory.bomb
        projectile.physicsBody?.collisionBitMask = PhysicsCategory.none
        projectile.physicsBody?.usesPreciseCollisionDetection = true
        
        // 3 - Determine offset of location to projectile
        let offset = touchLocation - projectile.position
        
        // 4 - Bail out if you are shooting down or backwards
        if offset.y < 0 { return }
        
        run(SKAction.playSoundFileNamed("Missle.mp3", waitForCompletion: false))
        // 5 - OK to add now - you've double checked position
        addChild(projectile)
        
        // 6 - Get the direction of where to shoot
        let direction = offset.normalized()
        
        // 7 - Make it shoot far enough to be guaranteed off screen
        let shootAmount = direction * 1000
        
        // 8 - Add the shoot amount to the current position
        let realDest = shootAmount + projectile.position
        
        // 9 - Create the actions
        let actionMove = SKAction.move(to: realDest, duration: 1.4)
        let actionMoveDone = SKAction.removeFromParent()
        projectile.run(SKAction.sequence([actionMove, actionMoveDone]))
    }
    func projectileDidCollideWithbomb(projectile: SKSpriteNode, bomb: SKSpriteNode) {
         print("Hit")
        run(SKAction.playSoundFileNamed("Bom.mp3", waitForCompletion: true))
        
    projectile.removeFromParent()
    
        bomb.removeFromParent()
    
        bombDestroyed += 1

        if bombDestroyed > 0 {

            let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
            let gameOverScene = GameOverScene(size: self.size, won: 2, monstersDestroyed: monstersDestroyed)
            view?.presentScene(gameOverScene, transition: reveal)
        }
    }
    
    
    
    func projectileDidCollideWithMonster(projectile: SKSpriteNode, monster: SKSpriteNode) {
//        print("Hit")
        
        run(SKAction.playSoundFileNamed("Blast.mp3", waitForCompletion: false))
        projectile.removeFromParent()
        monster.removeFromParent()
        
        monstersDestroyed += 1
        
        //HIGH SCORE
        func saveHighScore() {
            UserDefaults.standard.set(monstersDestroyed, forKey: "HIGHSCORE")
        }
        
        if monstersDestroyed > UserDefaults().integer(forKey: "HIGHSCORE") {
            saveHighScore()
        }
        
    }
    
}

extension GameScene: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        // 1
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask > contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB

        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        
        
        
        if ((firstBody.categoryBitMask & PhysicsCategory.projectile == 0b11) &&
            (secondBody.categoryBitMask & PhysicsCategory.monster == 0b10)) {
            if let monster = firstBody.node as? SKSpriteNode,
                let projectile = secondBody.node as? SKSpriteNode {
                projectileDidCollideWithMonster(projectile: projectile, monster: monster)
            }
        }
        // 2
        if ((firstBody.categoryBitMask & PhysicsCategory.projectile == 0b11) &&
            (secondBody.categoryBitMask & PhysicsCategory.bomb == 0b1)) {
            if let bomb = firstBody.node as? SKSpriteNode,
                let projectile = secondBody.node as? SKSpriteNode {
                projectileDidCollideWithbomb(projectile: projectile, bomb: bomb)
            }
        }
        
       
}
}
