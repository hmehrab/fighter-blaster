//
//  GameOverScene.swift
//  IOS.Pass
//
//  Created by Mehrab Hasan on 7/11/18.
//  Copyright © 2018 Mehrab Hasan. All rights reserved.
//



import UIKit
import SpriteKit

class GameOverScene: SKScene {
    var gameLogo: SKLabelNode!
    var playButton: SKShapeNode!
    var scoreNode : SKLabelNode!
    var bg : SKSpriteNode!
    var message: String!
   
    
    init(size: CGSize, won: Int, monstersDestroyed: Int) {
                super.init(size: size)
        
        scoreNode = SKLabelNode(fontNamed: "American Typewriter")
        scoreNode.text = "Yor have destroyed \(monstersDestroyed) enemy ships"
        scoreNode.fontSize = 20
        scoreNode.fontColor = SKColor.white
        scoreNode.position = CGPoint(x: size.width/2, y: size.height/1.2)
        self.addChild(scoreNode)
        
        let label = SKLabelNode(fontNamed: "American Typewriter")
        switch won {
        case 1:
            label.text = "Oh NO! Your territory is under attack. You just failed. Better luck next time!"

        case 2:
            label.text = " You just hit the wrong target. Better luck next time!"

            
        
        default:

            
            label.text = "You just failed. Better luck next time!"
        }
            

                label.numberOfLines = 5
        
                label.preferredMaxLayoutWidth = self.size.width - 50
                label.fontSize = 20
                label.fontColor = SKColor.white
                label.position = CGPoint(x: size.width/2, y: size.height/2.3)
                self.addChild(label)
        
        
        bg = SKSpriteNode(imageNamed: "skyy")
        bg.position = CGPoint(x: 0, y: 0)
        bg.size.width = self.frame.size.width
        bg.size.height = self.frame.size.height
        bg.anchorPoint = CGPoint(x: 0, y: 0)
        self.addChild(bg)
        
        // Add gameLabel
        let gameLabel = SKLabelNode(fontNamed: "American Typewriter")
        gameLabel.text = "Mission Failed"
        gameLabel.fontColor = SKColor.white
        gameLabel.fontSize = 40
        
        gameLabel.position = CGPoint(x: size.width/2, y: size.height/1.8)
        self.addChild(gameLabel)
        
        
        
        // Add play button
        
        playButton = SKShapeNode()
        playButton.name = "play_button"
        playButton.zPosition = 1
        playButton.position = CGPoint(x: size.width/2, y: size.height/3.1)

        playButton.fillColor = SKColor.lightGray
        let topCorner = CGPoint(x: -35, y: 35)
        let bottomCorner = CGPoint(x: -35, y: -35)
        let middle = CGPoint(x: 35, y: 0)
        let path = CGMutablePath()
        path.addLine(to: topCorner)
        path.addLines(between: [topCorner, bottomCorner, middle])
        playButton.path = path
        self.addChild(playButton)
        
        
    }
    
    // Play Again when play button is tapped
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            let touchedNode = self.nodes(at: location)
            for node in touchedNode {
                if node.name == "play_button" {
                    startGame()
                }
            }
        }
    }
    
    func startGame() {
        print("start game")
        
        playButton.run(SKAction.scale(to: 0, duration: 0.3)) {
            self.playButton.isHidden = true
        }
        
        
       
        
        let transition : SKTransition = SKTransition.fade(withDuration: 0.5)
        let scene = GameScene(size: size)
        scene.scaleMode = .resizeFill
        self.view?.presentScene(scene, transition: transition)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
